﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPL_Project_Enterprise
{
    public class ProductRepository : Repository<Product>
    {
        public override ProductRepository Model { get; set; }

        public override ProductRepository Detail(dynamic result)
        {
            var entity = new ProductRepository()
            {
                Id = result["Id"].ToString() as string,
                Description = result["Description"].ToString() as string,
            };
            return entity;
        }

        public override string Query(ProductRepository entity = null)
        {
            var query = string.Empty;

            switch (Verb)
            {
                case DataVerbs.Get:
                    query = "";
                    break;
                case DataVerbs.Post:
                    query = string.Format("", entity.Id, entity.Description);
                    break;
                case DataVerbs.Put:
                    query = string.Format("", entity.Id, entity.Description);
                    break;
                case DataVerbs.Delete:
                    query = string.Format("", entity.Id);
                    break;
                case DataVerbs.Generate:
                    query = "";
                    break;
            }
            return query;
        }
    }
}
